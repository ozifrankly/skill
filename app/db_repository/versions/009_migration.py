from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
level = Table('level', pre_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('description', String),
)

skill = Table('skill', post_meta,
    Column('candidate_id', Integer, primary_key=True, nullable=False),
    Column('knowledge_id', Integer, primary_key=True, nullable=False),
    Column('level', String(length=20)),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['level'].drop()
    post_meta.tables['skill'].columns['level'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['level'].create()
    post_meta.tables['skill'].columns['level'].drop()
