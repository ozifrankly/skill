from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
candidate = Table('candidate', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('name', String(length=64)),
    Column('email', String(length=120)),
    Column('twitter', String),
)

knowledge = Table('knowledge', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('description', String(length=64)),
)

level = Table('level', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('description', String(length=64)),
)

skill = Table('skill', post_meta,
    Column('candidate_id', Integer, primary_key=True, nullable=False),
    Column('knowledge_id', Integer, primary_key=True, nullable=False),
    Column('level_id', Integer),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['candidate'].create()
    post_meta.tables['knowledge'].create()
    post_meta.tables['level'].create()
    post_meta.tables['skill'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['candidate'].drop()
    post_meta.tables['knowledge'].drop()
    post_meta.tables['level'].drop()
    post_meta.tables['skill'].drop()
