# -*- coding: utf-8 -*-
from app import db
from sqlalchemy import Table, Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Candidate(db.Model):
    __tablename__ = 'candidate'
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(64), index = True)
    email = db.Column(db.String(120), unique = True)
    twitter = db.Column(db.String, unique = True)
    skills = relationship("Skill")

def __repr__(self):
    return '<Candidate %r>' % (self.name)

class Knowledge(db.Model):
    __tablename__ = 'knowledge'
    id = db.Column(db.Integer, primary_key = True)
    description = db.Column(db.String(64,convert_unicode=True), unique = True)

def __repr__(self):
    return '<Description %r>' % (self.description)

class Skill(db.Model):
    __tablename__ = 'skill'
    candidate_id = Column(Integer, ForeignKey('candidate.id'),primary_key=True)
    knowledge_id = Column(Integer,ForeignKey('knowledge.id'),primary_key=True)
    level = Column(String(20))
    knowledge = relationship("Knowledge",uselist=False)

