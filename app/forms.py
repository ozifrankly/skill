# -*- coding: utf-8 -*-
from flask import session
from flask.ext.wtf import Form
from wtforms import validators, TextField, SubmitField, SelectMultipleField, ValidationError, widgets
from wtforms.ext.sqlalchemy.validators import Unique
from models import Candidate,Knowledge
from app import db

def get_session():
    return db.session


class CandidateForm(Form):
    name = TextField("Nome",[validators.required("Preencha nome")])
    email = TextField("Email",[validators.required("Preencha email"),
                               validators.Email("Email inválido"),
                               Unique(get_session,Candidate,Candidate.email,"Email já cadastrado")])
    twitter = TextField("Twitter",[Unique(get_session,Candidate,Candidate.twitter,"Twitter já cadastrado")])
    submit = SubmitField("Enviar")




