# -*- coding: utf-8 -*-
from flask import render_template, session, request, flash
from app import app, db, models, forms


@app.route('/',methods=['GET','POST'])
def index():
    if request.method == 'POST' :
        form = forms.CandidateForm(request.form)
        
        if form.validate_on_submit():
            candidate = models.Candidate(name=form.name.data,email=form.email.data,twitter=form.twitter.data)
            for knowledge_model in models.Knowledge.query.all():
                skill = models.Skill(level=request.form[knowledge_model.description])
                skill.knowledge = knowledge_model
                candidate.skills.append(skill)

            db.session.add(candidate)
            db.session.commit()
            return render_template('success.html',candidate=candidate)
        else:
            return render_template('form.html',form=form)
    elif request.method == 'GET':
        form = forms.CandidateForm()
        return render_template('form.html',form=form)


@app.route('/report',methods=['GET'])
def report():
    candidates = models.Candidate.query.all()
    return render_template('report.html',candidates=candidates)


@app.route('/show')
def show():
    id = request.args['id']
    candidate = models.Candidate.query.get(id)
    return render_template("show.html",candidate = candidate)

if __name__ == '__main__':
    app.run()

